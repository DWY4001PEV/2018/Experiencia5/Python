mensaje = "Hola mundo!"
texto = "texto largo"
caracter = "c"
numero = 23

#esto es un comentario de una línea

print("Saludo: " , mensaje)

lista = [1,"dos",numero,[10,20,30]]

print("Primer elemento: " , lista[0])
print("Segundo elemento: ", lista[1])
print("Tercer elemento: ", lista[2])
print("Cuarto elemento: ", lista[3])
print("Último elemento: ", lista[-1])
print("Penúltimo elemento: ", lista[-2])

diccionario = {"perro":"patas",
                "gaviota":"alas",
                "trucha":"aletas"}

print("los perros tienen  : ",diccionario["perro"])
print("las gaviotas tienen: ",diccionario["gaviota"])
print("las truchas tienen : ",diccionario["trucha"])

persona = {"nombre":"juan","apellido":"perez"}

print("Nombre: " , persona["nombre"])
print("Apellido: " , persona["apellido"])

if numero < 0:
    print ("Negativo")
elif numero > 0:
    print ("Positivo")
else:
    print ("Cero")

print("Mensaje fuera del if")
'''
edad = 0
while edad < 18:
    edad = edad + 1
    print ("Felicidades, tienes", edad)

while True:
    entrada = input("> ")
    if entrada == "adios":
        break
    else:
       print (entrada)
'''
print("\nUso de For")
print("**********")
secuencia = ["uno", "dos", "tres"]
for elemento in secuencia:
    print (elemento)

print("\nFunciones")
print("**********")
def mi_funcion(param1, param2):
    print (param1)
    print (param2)
    #este comentario está dentro de la función mi_funcion

#este comentario está fuera de la función mi_funcion

mi_funcion("prueba", 4)

print("\nParametros dinamicos")
print("**********")
def varios(param1, param2, *otros):
    print(param1)
    print(param2)
    for val in otros:
        print (val)

varios(1, 2)
varios(1, 2, 3)
varios(1, 2, 3, 4)
